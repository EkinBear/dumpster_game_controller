#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import json
from std_srvs.srv import Trigger, TriggerResponse
import time
import os
import requests

class GameController:

    def __init__(self, robot_name):

        self.robot_status = "start"
        self.dumpsters_remaining = 16
        self.wheelie_bins_remaining = 4
        self.dumpsters_collected = 0
        self.wheelie_bins_collected = 0

        self.metrics = {
            'dumpsters_remaining': str(self.dumpsters_remaining),
            'wheelie_bins_remaining': str(self.wheelie_bins_remaining),
            'dumpsters_collected': str(self.dumpsters_collected),
            'wheelie_bins_collected': str(self.wheelie_bins_collected),
            'status': self.robot_status,
        }

        self.robot_name = robot_name

        rospy.init_node("game_controller")

        self.metric_pub = rospy.Publisher('simulation_metrics', String, queue_size=10)
        metric_msg_rate = 10
        rate = rospy.Rate(metric_msg_rate)

        rospy.Subscriber("collision", String, self.contact_callback)

        s = rospy.Service('/robot_handler', Trigger, self.handle_robot)

        while ~rospy.is_shutdown():
            if self.dumpsters_collected == 1:
                self.achieve(56)
            if self.dumpsters_collected == 4:
                self.achieve(57)
            if self.dumpsters_collected == 16:
                self.achieve(60)
            if self.wheelie_bins_collected == 4:
                self.achieve(61)
            self.publish_metrics()
            rate.sleep()
            if self.robot_status == "stop":
                break


    def handle_robot(self,req):
        if self.robot_status == "start":
            self.robot_status = "run"
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "run":
            time.sleep(0.1)

            if self.dumpsters_remaining <= 0 and self.wheelie_bins_remaining <= 0:
                self.metrics['status'] = "collected_all_garbage"
                self.robot_status = "stop"
                self.publish_metrics()

                return TriggerResponse(
                    success = True,
                    message = self.robot_status
                )

            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "stop":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )


    def publish_metrics(self):
        self.metrics['dumpsters_remaining'] = str(self.dumpsters_remaining)
        self.metrics['wheelie_bins_remaining'] = str(self.wheelie_bins_remaining)
        self.metrics['dumpsters_collected'] = str(self.dumpsters_collected)
        self.metrics['wheelie_bins_collected'] = str(self.wheelie_bins_collected)
        self.metrics['status'] = str(self.robot_status)
        self.metric_pub.publish(json.dumps(self.metrics, sort_keys=True))


    def contact_callback(self, data):
        collision = data.data.split(":")[1]
        if "dumpster" in collision:
            self.dumpsters_remaining -= 1
            self.dumpsters_collected += 1
        elif "wheelie" in collision:
            self.wheelie_bins_remaining -= 1
            self.wheelie_bins_collected += 1
    
    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })


if __name__ == '__main__':
    # Name of robot
    controller = GameController("dumpster_truck")
